#include "TutorialApplication.h"

std::string gstreamconf = "v4l2src device=/dev/video0 use-fixed-fps=false ! ffmpegcolorspace ! capsfilter caps=video/x-raw-rgb,bpp=24,width=640,height=480 ! identity name=artoolkit ! fakesink";

//-------------------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
    physicalCamera = new CameraDriver(gstreamconf,"Data/camera_para.dat","Data/patt.hiro");
}
//-------------------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
    physicalCamera->cleanup();
    physicalCamera->cancel();
    delete physicalCamera;
}

//-------------------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    //mCamera->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
    //mCamera->setNearClipDistance(0.01f);
    //mCamera->setFarClipDistance(10.0f);
    // Set the scene's ambient light
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));

    // Create an Entity
    Ogre::Entity* ogreHead = mSceneMgr->createEntity("Head", "PS1.mesh");

    // Create a SceneNode and attach the Entity to it
    Ogre::SceneNode* headNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("HeadNode");
    headNode->attachObject(ogreHead);
    headNode->setPosition(0, 0, 0);
    headNode->scale(20, 20, 20);

    // Create a Light and set its position
    Ogre::Light* light = mSceneMgr->createLight("MainLight");
    light->setPosition(20.0f, 80.0f, 50.0f);

    /* Setup the background */
    // Create the texture
    videoTexture = Ogre::TextureManager::getSingleton().createManual(
        "DynamicTexture", // name
        Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        Ogre::TEX_TYPE_2D,      // type
        640, 480,         // width & height
        0,                // number of mipmaps
        Ogre::PF_BYTE_BGRA,     // pixel format
        Ogre::TU_DEFAULT);      // usage; should be TU_DYNAMIC_WRITE_ONLY_DISCARDABLE for
                          // textures updated very often (e.g. each frame)

    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("DynamicTexture");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

    // Create background rectangle covering the whole screen
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("Background");

    // Render the background before everything else
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

    // Use infinite AAB to always stay visible
    Ogre::AxisAlignedBox aabInf;
    aabInf.setInfinite();
    rect->setBoundingBox(aabInf);

    // Attach background to the scene
    mSceneMgr->getRootSceneNode()->createChildSceneNode("Background")->attachObject(rect);

    //setup the camera:
//    double pmat[16];
//    physicalCamera->getProjectionMatrix(pmat);
//    Ogre::Matrix4 projectionMatrix(pmat[0], pmat[1], pmat[2], pmat[3],
//                                   pmat[4], pmat[5], pmat[6], pmat[7],
//                                   pmat[8], pmat[9], -pmat[10],-pmat[11],
//                                   pmat[12],pmat[13],pmat[14],pmat[15]);
//    projectionMatrix = projectionMatrix.transpose();
//    for (int i = 0; i < 4; i++) {
//        for (int j = 0; j < 4; j++)
//            std::cout << projectionMatrix[i][j] << ", ";
//        std::cout << std::endl;
//    }
    //mSceneMgr->getCamera("PlayerCam")->setCustomProjectionMatrix(true, projectionMatrix);
}

bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& evt) {
    Ogre::SceneNode* node = mSceneMgr->getSceneNode("HeadNode");
    double mat[3][4];
    physicalCamera->getTransformationMatrix(mat);
    Ogre::Matrix4 m( mat[0][0],  mat[0][1],  mat[0][2],  mat[0][3],
                    -mat[1][0], -mat[1][1], -mat[1][2], -mat[1][3],
                    -mat[2][0], -mat[2][1], -mat[2][2], -mat[2][3],
                     0.0,        0.0,        0.0,        1.0     );
    //m = m.inverseAffine();
    if (!m.getTrans().isNaN()) {
        node->setPosition(m.getTrans());
    }
    Ogre::Quaternion quat = m.extractQuaternion();
    Ogre::Quaternion reorient(Ogre::Degree(180.0), Ogre::Vector3::UNIT_X);
    quat = quat * reorient;
    node->setOrientation(quat);

    //set the background
    // Get the pixel buffer
    Ogre::HardwarePixelBufferSharedPtr pixelBuffer = videoTexture->getBuffer();

    // Lock the pixel buffer and get a pixel box
    pixelBuffer->lock(Ogre::HardwareBuffer::HBL_NORMAL); // for best performance use HBL_DISCARD!
    const Ogre::PixelBox& pixelBox = pixelBuffer->getCurrentLock();

    Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixelBox.data);

    // Fill in some pixel data. This will give a semi-transparent blue,
    // but this is of course dependent on the chosen pixel format.
    ARUint8 *cameraData = physicalCamera->getFrame();
    for (size_t j = 0; j < 480; j++) {
        for(size_t i = 0; i < 640*3; i += 3)
        {
            *pDest++ = cameraData[(j*640*3)+i+2]; // B
            *pDest++ = cameraData[(j*640*3)+i+1]; // G
            *pDest++ = cameraData[(j*640*3)+i]; // R
            *pDest++ = 255; // A
        }
    }

    // Unlock the pixel buffer
    pixelBuffer->unlock();
    return BaseApplication::frameRenderingQueued(evt);
}


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif
