#include "CameraDriver.h"
#include <iostream>
#include <stdlib.h>

#define RANGE 1e25

CameraDriver::CameraDriver(std::string config, std::string cameraParams, std::string pattern) :
    Thread()
{
    ARParam wparam;
    /* open the video path */
    if (arVideoOpen((char*)config.c_str()) < 0) {
        std::cerr << "Failed to open video stream!" << std::endl;
        exit(0);
    }
    /* find the size of the video */
    int xsize, ysize;
    if (arVideoInqSize(&xsize, &ysize) < 0) {
        std::cerr << "Failed to determine video size!" << std::endl;
        exit(0);
    }
    std::cout << "Image size (x,y) = (" << xsize << ", " << ysize << ")" << std::endl;

    /* set the initial camera parameters */
    if (arParamLoad(cameraParams.c_str(), 1, &wparam) < 0) {
        std::cerr << "Camera parameter load error !!" << std::endl;
        exit(0);
    }
    arParamChangeSize(&wparam, xsize, ysize, &m_cameraParameters);
    arInitCparam(&m_cameraParameters);
    std::cout << "*** Camera Parameter ***" << std::endl;
    arParamDisp(&m_cameraParameters);

    if ((m_patternID = arLoadPatt(pattern.c_str())) < 0) {
        std::cerr << "Pattern load error !!" << std::endl;
        exit(0);
    }
    arVideoCapStart();
    start();
}

void CameraDriver::getTransformationMatrix(double mat[3][4]) {
    m_mutex.lock();
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 4; j++)
            mat[i][j] = m_transformationMatrix[i][j];
    m_mutex.unlock();
}

void CameraDriver::getProjectionMatrix(double mat[16]) {
    m_mutex.lock();
    argConvGLcpara(&m_cameraParameters, 1.0/RANGE, RANGE, mat);
    m_mutex.unlock();
}

ARUint8* CameraDriver::getFrame() {
    ARUint8 *result;
    m_mutex.lock();
    result = m_frame;
    m_mutex.unlock();
    return result;
}

void CameraDriver::cleanup() {
    arVideoCapStop();
    arVideoClose();
}

void CameraDriver::run() {
    ARMarkerInfo *markers;
    double (*prevTransformationMatrix)[4];
    int numDetected;

    while (true) {
        m_mutex.lock();
        /* grab a video frame */
        if ((m_frame = (ARUint8 *)arVideoGetImage()) == NULL) {
            arUtilSleep(2);
        } else {
            /* detect the markers in the video frame */
            if( arDetectMarker(m_frame, 100, &markers, &numDetected) < 0 ) {
                std::cerr << "Failed to detect markers!" << std::endl;
                cleanup();
                exit(0);
            }
            /* check for object visibility */
            int k = -1, j;
            for(j = 0; j < numDetected; j++) {
                if(m_patternID == markers[j].id) {
                    if( k == -1 )
                        k = j;
                    else if(markers[k].cf < markers[j].cf)
                        k = j;
                }
            }
            if (k != -1) {
                // found marker!
                double patt_center[2] = {0.0, 0.0};
                arGetTransMatCont(&markers[k], prevTransformationMatrix, patt_center, 80.0, m_transformationMatrix);
                prevTransformationMatrix = m_transformationMatrix;
            }
            arVideoCapNext();
        }
        m_mutex.unlock();
        arUtilSleep(42);
    }
}

CameraDriver::~CameraDriver() {
    if (m_frame)
        delete m_frame;
}
