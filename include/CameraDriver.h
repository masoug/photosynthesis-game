#ifndef CAMERADRIVER_H
#define CAMERADRIVER_H

#include <string>
#include <AR/gsub.h>
#include <AR/video.h>
#include <AR/param.h>
#include <AR/ar.h>
#include <AR/gsub_lite.h>
#include <OpenThreads/Thread>
#include <OpenThreads/Mutex>
#include <vector>

class CameraDriver : public OpenThreads::Thread
{
    public:
        CameraDriver(std::string config, std::string cameraParams, std::string pattern);
        void getTransformationMatrix(double mat[3][4]);
        void getProjectionMatrix(double mat[16]);
        ARUint8* getFrame();
        void cleanup();
        ~CameraDriver();

    private:
        void run();
        ARParam m_cameraParameters;
        ARUint8 *m_frame;
        double m_transformationMatrix[3][4];
        OpenThreads::Mutex m_mutex;
        int m_patternID;
};

#endif // CAMERADRIVER_H
